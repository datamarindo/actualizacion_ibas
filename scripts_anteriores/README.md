```r
library(sf)
library(dplyr)

aves = read_sf("aves_aicas_no_calificadas.gpkg")
# un subconjunto
aves = aves %>% filter(scientific_name == "Actitis macularius")
# 1 km² es un radio de 564 m
ea = st_is_within_distance(aves, aves, 564.1896)

# AQUÍ VEMOS QUE LAS 3,4,5,6,7 están a menos de 564

print(ea)
# Sparse geometry binary predicate list of length 80, where the predicate was `is_within_distance'
# first 10 elements:
#  1: 1
#  2: 2
#  3: 3, 4, 5, 6, 7
#  4: 3, 4, 5, 6, 7
#  5: 3, 4, 5, 6, 7
#  6: 3, 4, 5, 6, 7
#  7: 3, 4, 5, 6, 7

unicas = unique(ea)
print(unicas)
# [[1]]
# [1] 1
# 
# [[2]]
# [1] 2
# 
# [[3]]
# [1] 3 4 5 6 7
# [...]

# sacamos el primer elemento de cada unicas[[]]
listado_unicas = sapply(unicas, function(x) x[1])

# visualizamos
plot(aves[listado_unicas,0], graticule = T, axes = T, pch = 17, cex = 0.6, xlim = c(-99.97, -99.93), 
     ylim = c(26.3,26.34))
plot(aves[0], col = 2, add = T)
legend("topright",c("filtrado", "todos"), pch = c(17,1), col = c(1,2))
``` 

![funcionamiento](Rplot.png)


```r
# probamos el nuevo objeto con st_within_distance()
prueba = st_is_within_distance(aves[listado_unicas,], aves[listado_unicas,], 564.189)
# ninguna length debe ser mayor a 1
lengths(prueba) #  :)
#[1] 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
```
