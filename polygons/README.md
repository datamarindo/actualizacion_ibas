# Archivos geoespaciales utilizados

1. __Mexico_IBAs__ es el conjunto de polígonos __1)__ limpiados de traslapes, __2)__ con ortografía correcta de las toponimias, __3)__ seleccionados de entre BirdLife y Conabio el que mejor cubriera las áreas de interés;
1. __AICAS no calificadas__ son los polígonos de las AICAS que no estaban aprobadas en revisiones pasadas; se incluyen porque se encontro que usando datos de eBird algunas de estas AICAS cumplen con los criterios de BirdLife.
1. __CONTINENTE__ los polígonos viejos y nuevos de los monitoreos de aves acuáticas del CICESE en México continental.
1. __PENINSULA__ al igual que CONTINENTE, pero para la península de Baja California.
1. __Polígonos y puntos PRONOR__ Zonas y puntos de muestreo de aves acuáticas de Pronatura Noroeste
