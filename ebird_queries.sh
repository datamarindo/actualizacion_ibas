# ogr2ogr tenía una complicación
# severa al enfrentar el separado por tabuladores
# de ebird; por lo tanto se genera primero un csv 
# conformante separado por comas


# solo cambiando el separador de tabulador por la coma y
# tomando solo las columnas que nos interesan

# MEMORY EFFICIENT :)
#---------------------------------------------------------------
# 1: cambio a coma; filtro a category="species" y date ">=" 2010

csvcut -t -c "SCIENTIFIC NAME","COMMON NAME","OBSERVATION COUNT","OBSERVATION DATE","CATEGORY","LONGITUDE","LATITUDE" ebd_MX_relAug_2020.csv | csvgrep -c "OBSERVATION DATE" -r 20[12] | csvgrep -c "CATEGORY" -m "species"  > aves_mex_species_2010.csv

#real	4m55.227s
#Feature Count: 8374834

# QUITAR DUPLICADOS --------------------------------------------

ogr2ogr -f CSV aves_mex_species_2010_sin_dups.csv -dialect sqlite -sql 'select distinct scientific_name, observation_count, observation_date, year, CATEGORY, LONGITUDE, LATITUDE from (select "SCIENTIFIC NAME" as scientific_name, cast(replace("OBSERVATION COUNT", "X", "1") AS integer) as observation_count, "OBSERVATION DATE" as observation_date, strftime("%Y", "OBSERVATION DATE") as year, "CATEGORY","LONGITUDE","LATITUDE" from "aves_mex_species_2010")' aves_mex_species_2010.csv

#real	5m6.496s
#Feature Count: 5731192

# ogr2ogr pone comillas a los campos numéricos ¿?¿? quién sabe por qué
# se quitan con AUTODETECT_TYPE=YES

# convertir a GPKG -----------------------------------------------
ogr2ogr -f GPKG aves_mex.gpkg -a_srs EPSG:4326 -oo X_POSSIBLE_NAMES=LONG* -oo Y_POSSIBLE_NAMES=LAT* -oo KEEP_GEOM_COLUMNS=NO -nln aves_mex -oo AUTODETECT_TYPE=YES aves_mex_species_2010_sin_dups.csv

#real	7m9.445s
#Feature Count: 5731192


# JUNTAR CAPAS EN EL GEOPACKAGE------------------------
# aicas no calificadas:
ogr2ogr -f GPKG aves_mex.gpkg -update -nln aicas_no_calificadas polygons/aicas_no_calificadas.gpkg

# mexico ibas:
ogr2ogr -f GPKG aves_mex.gpkg -update -nln mexico_ibas polygons/mexico_ibas.gpkg


# ST_JOINS -------------------------------------------
# aicas_no calificadas:
ogr2ogr -f CSV aves_aicas_no_calificadas.csv -dialect sqlite -sql "select a.*, b.*, st_x(a.geom) as lon, st_y(a.geom) as lat from aves_mex a, aicas_no_calificadas b where st_intersects(a.geom, b.geom)" aves_mex.gpkg 

real	22m9.876s

# con subdivide no mejoró :(
time ogr2ogr -f CSV aves_aicas_no_calificadas_subdi.csv -dialect sqlite -sql "select a.*, b.NOMBRE, st_x(a.geom) as lon, st_y(a.geom) as lat from aves_mex a, aicas_subdi b where st_intersects(a.geom, b.geom)" aves_mex.gpkg 

real	33m4.573s


# Mexico ibas:
time ogr2ogr -f CSV aves_mex_ibas.csv -dialect sqlite -sql "select a.*, b.*, st_x(a.geom) as lon, st_y(a.geom) as lat from aves_mex a, mexico_ibas b where st_intersects(a.geom, b.geom) = 1" aves_mex.gpkg

# estuvo 307m53.291s y sacó:
1533394 registros

# QGIS
{ 'DISCARD_NONMATCHING' : False, 'INPUT' : '/home/elio/actualizacion_ibas/aves_mex.gpkg|layername=aves_mex', 'JOIN' : '/home/elio/actualizacion_ibas/aves_mex.gpkg|layername=mexico_ibas', 'JOIN_FIELDS' : [], 'METHOD' : 0, 'OUTPUT' : '/home/elio/aves_mex_ibas.gpkg', 'PREDICATE' : [0], 'PREFIX' : '' }
Ejecución completada en 654.02 segundos
Resultados:
{'JOINED_COUNT': 1974758,

