# Informe de actividades realizadas como parte del proceso de actualización de IBAs en México"
Elio Lagunes Díaz, Efraín Castillejos Castellanos

---

## Introducción

     Las Áreas de Importancia para las Aves y la Biodiversidad son zonas delimitadas geográficamente, de alto valor para la conservación de especies de aves, ya sea porque albergan especies amenazadas o endémicas o porcentajes altos de la población total de una especie. La autoridad encargada de decretar las IBAs es BirdLife, en conjunto con socios nacionales en los distintos países, tal es el caso de Pronatura en México. Las IBAs llevan un proceso periódico de actualización, a lo que obedece el presente trabajo, llevado a cabo durante los meses de Diciembre 2020 a Junio 2021. El proyecto fue realizado por un equipo conformado por personal de Pronatura Sur y Pronatura Veracruz, asistido por personal de Birdlife. 

     El proceso incluyó una comparación-revisión de los límites entre los polígonos de las IBAs y las AICAs, seguido por una homologación de los nombres científicos usados en los registros biológicos y en los listados de los criterios de BirdLife. El cumplimiento de dichos criterios fue revisado utilizando distintas metodologías cuyos propósitos principales eran **1)** Evitar sobreestimar las poblaciones;  **2)** Utilizar la mayor cantidad de datos disponibles; **3)** Que fuera un proceso replicable, escalable, programático y auditable.


## Materiales y metodología

     Los polígonos de las Áreas de Importancia para la Conservación de las Aves (AICA) fueron obtenidos del geoportal de Conabio (http://www.conabio.gob.mx/informacion/gis/), mientras que los polígonos de las IBAs fueron proporcionados por BirdLife, además se agregaron 14 polígonos de áreas de aves acuáticas para la su consideración como IBAs nuevas. Además de los polígonos mencionados, se utilizaron los de AICAs que previamente no habían calificado como IBAs, para revisar si era posible que calificaran en este proceso.

     Los registros biológicos provinieron principalmente de la _eBird Basic Dataset_ de http://ebird.org, así como del monitoreo Western Mexico Waterbird Survey (2011-2020) (proporcionados por Eduardo Palacios de CICESE), del monitoreo de aves acuáticas de Pronatura Noroeste (2013-2017) y del programa Veracruz Río de Rapaces (2010-2020) de Pronatura Veracruz.

     La información de categorías de riesgos, tamaños poblacionales globales y tamaños de rango geográfico por especie fueron provistos por BirdLife, así como los umbrales poblacionales requeridos por los criterios para ser consideradas como IBAs. Antes de la revisión de los criterios se hicieron una serie de preprocesos para hacer los datos compatibles y garantizar que estuvieran correctos.

### Preproceso geoespacial

     Antes de unir geográficamente el polígono de la IBA a los registros geográficos que cayeran dentro del límite de una, se llevó a cabo una revisión de los límites, comparando los de Birdlife con los de AICAs de Conabio, reconociendo a la segunda como la autoridad dentro de México; también se compararon con las zonas geográficas a las que se refieren, por ejemplo, se revisó que la IBA Lago de Texcoco siguiera los límites del cuerpo de agua de ese mismo nombre. Para facilitar y agilizar esta tarea se hizo un mapa interactivo, que permitía buscar por nombre las IBAs y visualizar ambos polígonos, junto con una imagen satelital para revisar que cubriera el área de interés, el mapa está disponible en: https://pronaturaveracruz.org/recursos/aicas_ibas.html

     Después de este proceso se encontró que muchas IBAs presentaban un desfase ligero, de entre 50 m y 90 m, respecto de la AICA de su mismo nombre, probablemente originados por una transferencia y conversión de datos entre instituciones y personas dentro de las instituciones (fig. \ref{desfase}); otras problemáticas se encontraron en IBAs donde BirdLife realizó una ampliación de límites (fig. \ref{modificado}), que los límites no coincidían con su área de interés o que cruzaban fronteras internacionales (tabla 1).

%![Desfase ligero entre polígonos: AICA (rojo) e IBA (azul), Isla Guadalupe\label{desfase}](figs/141a.png "li"){ width=15% }

%![Modificación de polígono por BirdLife: AICA (rojo) e IBA (azul), Ría Lagartos\label{modificado}](figs/186a.png "lix"){ width=25% }


:  Resumen de problemáticas detectadas como parte del proceso de revisión de polígonos

Problemática | cantidad
-------------|---------:
Desfase ligero | 105
Ampliación Birdlife | 10
No coincide con cuerpo de agua | 4
Cruza fronteras | 4

