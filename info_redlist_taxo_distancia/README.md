# Archivos empleados en el proceso de actualización


1. __IBA_Criteria_MX_2020__ Contiene la información del status actual de las especies en la RedList (para criterios A1 y B1), así como los valores de 1% de las poblaciones de rango restringido (criterio A2) y valores de 1% de poblaciones en general (para criterio A4)
1. __IBAs and Aicas master list__ es la relación de las IBAs aprobadas (qualified) y sus números de ID entre Conabio (ID_AICA) y Birdlife (SitRecID)
1. __Propuesta distancias__ Es la distancia en metros del buffer que se asigna a cada observación para reducir la duplicidad de registros cercanos
1. __Rosetta stone AOU Birdlife__ relaciona los nombre científicos entre eBird, AOU, Conabio, Birdlife y CICESE, para determinar su estado actual en IUCN así como para enlazar con los valores de __IBA_Criteria_MX_2020__
